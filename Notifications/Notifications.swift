//
//  Notifications.swift
//  Notifications
//
//  Created by Александр Полочанин on 22.08.23.
//  Copyright © 2023 Alexey Efimov. All rights reserved.
//

import UIKit

class Notifications: NSObject, UNUserNotificationCenterDelegate {
    
    let notificationCenter = UNUserNotificationCenter.current()

    
    //Создаем метод для устаногвки запроса на использования уведомлений
    func requestAutorization() {
        notificationCenter.requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in
            print("Permission granted: \(granted)")
            
            guard granted else { return }
            self.getNotificationSettings()
            
        }
    }

    //Для отслеживания настроек создаем метод
    func getNotificationSettings() {
        notificationCenter.getNotificationSettings { settings in
            print("Notification settings: \(settings)")
            
            
            //Проверка статуса авторизации, если статус подтвержден, то мы регистрируем приложение в качестве получателя удаленных уведомлений
            guard settings.authorizationStatus == .authorized else { return }
            
            
            DispatchQueue.main.async {
                //Проверяем статус авторизации, если пользователь предоставиль разрешение на получение уведомлений, то происходит регистрация на сервисе APNs
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    //Метод отвечает за расписание уведомлений
    func scheduleNotification(notificationType: String) {
        
        let content = UNMutableNotificationContent()
        let userAction = "User Action"
        
        content.title = notificationType
        content.body = "This is example how to create " + notificationType
        content.sound = UNNotificationSound.default
        content.badge = 1
        //Для того чтобы пользовательские действия стали доступны в уведомлении
        content.categoryIdentifier = userAction
        
        guard let path = Bundle.main.path(forResource: "iconTest", ofType: "png") else { return }
        
        let url = URL(fileURLWithPath: path)
        
        do {
            let attachment = try UNNotificationAttachment(identifier: "iconTest", url: url)
            
            content.attachments = [attachment]
        } catch {
            print("The attachment cold not be loaded")
        }
          
        
        
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        
        //Для каждого запроса требуется свой идентификатор
        let identifire = "Local Notification"
        let request = UNNotificationRequest(identifier: identifire, content: content, trigger: trigger)
        
        //Имея запрос можем вызвать в самом центре уведомлений
        notificationCenter.add(request) { error in
            if let error {
                print("Error \(error.localizedDescription)")
            }
        }


        //Позволит пользователю отложить уведомление на некоторое время
        let snoozeAction = UNNotificationAction(identifier: "Snooze", title: "Snooze", options: [])
        
        //Деструктивное действие
        let deleteAction = UNNotificationAction(identifier: "Delete", title: "Delete", options: [.destructive])
        
        //Определили категорию для действий и включили массив действий
        let category = UNNotificationCategory(
            identifier: userAction,
            actions: [snoozeAction, deleteAction],
            intentIdentifiers: [],
            options: []
        )

        //Зарегистрировали нашу категорию в центре уведомлений
        notificationCenter.setNotificationCategories([category])
    }
    
    //Получаем уведомления при открытом приложении
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        completionHandler([.alert, .sound])
    }
    
    
    
    //Обрабатываем полученные уведомления
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if response.notification.request.identifier == "Local Notification" {
            print("Handling notification with the local Notification Identifire")
        }
        
        switch response.actionIdentifier {
        case UNNotificationDismissActionIdentifier:
            print("Dismiss Action")
        case UNNotificationDefaultActionIdentifier:
            print("Default Action")
        case "Snooze":
            print("Snooze Action")
            scheduleNotification(notificationType: "Reminder")
        case "Delete":
            print("Delete Action")
        default:
            print("Unknown Action")
        }
        
        completionHandler()
    }

}
